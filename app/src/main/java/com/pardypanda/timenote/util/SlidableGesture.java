package com.pardypanda.timenote.util;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

/**
 * Created by Tanay on 18/11/15.
 */
public class SlidableGesture implements View.OnTouchListener {

    protected static final String TAG = "SlidableGesture";

    private static final int ACTION_TYPE_DEFAULT = 0;
    private static final int ACTION_TYPE_UP = 1;
    private static final int ACTION_TYPE_RIGHT = 2;
    private static final int ACTION_TYPE_DOWN = 3;
    private static final int ACTION_TYPE_LEFT = 4;
    private static final int SLIDE_RANGE = 100;
    private static final int SLIDE_OFFSET = 300;
    private float mTouchStartPointX;
    private float mTouchStartPointY;
    private int mActionType = ACTION_TYPE_DEFAULT;

    private ListView mList;
    private Context mContext;

    public SlidableGesture(Context context, ListView list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int x = (int) event.getRawX();
        int y = (int) event.getRawY();

        float deltaX = 0;
        float deltaY = 0;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mTouchStartPointX = event.getRawX();
                mTouchStartPointY = event.getRawY();
                break;

            case MotionEvent.ACTION_MOVE:

                deltaX = mTouchStartPointX - x;
                deltaY = mTouchStartPointY - y;

                if (mTouchStartPointX - x > SLIDE_RANGE) {
                    mActionType = ACTION_TYPE_LEFT;
                    return true;
                } else if (x - mTouchStartPointX > SLIDE_RANGE) {
                    mActionType = ACTION_TYPE_RIGHT;
                    return true;
                } else if (mTouchStartPointY - y > SLIDE_RANGE) {
                    mActionType = ACTION_TYPE_UP;
                } else if (y - mTouchStartPointY > SLIDE_RANGE) {
                    mActionType = ACTION_TYPE_DOWN;
                }
                break;

            case MotionEvent.ACTION_UP:

                if (Math.abs(deltaY) < SLIDE_OFFSET) {
                    if (mActionType == ACTION_TYPE_RIGHT) {
                        slideToRight(getPostion(event));
                    } else if (mActionType == ACTION_TYPE_LEFT) {
                        slideToLeft(getPostion(event));
                    }
                } else if (Math.abs(deltaX) < SLIDE_OFFSET) {
                    if (mActionType == ACTION_TYPE_UP) {
                        slideUp(getPostion(event));
                    } else if (mActionType == ACTION_TYPE_DOWN) {
                        slideDown(getPostion(event));
                    }
                }
                break;

            default:
                break;
        }
        return false;
    }

    protected int getPostion(MotionEvent e1) {
        return mList.pointToPosition((int) e1.getX(), (int) e1.getY());
    }

    protected void slideToLeft(int position) {
        Log.d(TAG, "slideToLeft() was called.");
    }

    protected void slideToRight(int position) {
        Log.d(TAG, "slideToRight() was called.");
    }

    protected void slideUp(int position) {
        Log.d(TAG, "slideUp() was called.");
    }

    protected void slideDown(int position) {
        Log.d(TAG, "slideDown() was called.");
    }
}
