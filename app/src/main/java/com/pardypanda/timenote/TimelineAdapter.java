package com.pardypanda.timenote;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.pardypanda.timenote.api.STXPost;
import com.pardypanda.timenote.util.BlurBuilder;
import com.pardypanda.timenote.util.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Tanay on 06/11/15.
 */
public class TimelineAdapter extends BaseAdapter {

    Transformation transformationCirlce = new RoundedTransformationBuilder()
            .borderColor(Color.WHITE)
            .borderWidthDp(3)
            .cornerRadiusDp(30)
            .oval(false)
            .build();
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<STXPost> mListPosts;
    private Map<Integer, Integer> mListTimeViewState = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> mListItemSwipeState = new HashMap<Integer, Integer>();

    TimelineAdapter(Context context, List<STXPost> listPosts) {
        mContext = context;
        mListPosts = listPosts;

        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public void updateList(List<STXPost> updatedList) {
        this.mListPosts = updatedList;
        notifyDataSetChanged();
    }

    public int getSwipeState(int position) {
        if (mListItemSwipeState.get(position) != null)
            return mListItemSwipeState.get(position);
        else
            return 0;
    }

    public void setSwipeState(int position, int swipeState) {
        mListItemSwipeState.put(position, swipeState);
        notifyDataSetChanged();
    }

    public int getmListTimeViewState(int position) {
        if (mListTimeViewState.get(position) != null) {
            return mListTimeViewState.get(position);
        } else
            return 0;
    }

    public void setmListTimeViewState(int position, int state) {
        mListTimeViewState.put(position, state);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mListPosts.size();
    }

    @Override
    public Object getItem(int i) {
        return mListPosts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mListPosts.get(i).hashCode();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        // Setup ViewHolder
        ViewHolder viewHolder;

        if (view == null) {

            viewHolder = new ViewHolder();
            view = mLayoutInflater.inflate(R.layout.listitem_timeline, viewGroup, false);

            viewHolder.imgUserProfile = (ImageView) view.findViewById(R.id.imgUserProfile);
            viewHolder.txtUsername = (TextView) view.findViewById(R.id.txtUsername);
            viewHolder.txtPostShareType = (TextView) view.findViewById(R.id.txtPostShareType);

            viewHolder.layoutImgMain = (RelativeLayout) view.findViewById(R.id.layout_imgMain);
            viewHolder.layoutImgMainBase = (RelativeLayout) view.findViewById(R.id.layoutImgMainBase);
            viewHolder.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            viewHolder.imgMain = (ImageView) view.findViewById(R.id.imgMain);
            viewHolder.txtImgTitle = (TextView) view.findViewById(R.id.txtImgTitle);
            viewHolder.icMorePhotos = (ImageView) view.findViewById(R.id.icMorePhotos);
            viewHolder.icReminderBell = (ImageView) view.findViewById(R.id.icReminderBell);
            viewHolder.imgBlur = (ImageView) view.findViewById(R.id.imgBlur);

            viewHolder.icNoteStatus = (ImageView) view.findViewById(R.id.icNoteStatus);

            viewHolder.layoutCountDownTimer = (LinearLayout) view.findViewById(R.id.layoutCountdownTimer);
            viewHolder.txtCountdownIndicator = (TextView) view.findViewById(R.id.txtCountdownIndicator);
            viewHolder.txtCountdownNumber1 = (TextView) view.findViewById(R.id.txtCountdownNumber1);
            viewHolder.txtCountdownText1 = (TextView) view.findViewById(R.id.txtCountdownText1);
            viewHolder.txtCountdownNumber2 = (TextView) view.findViewById(R.id.txtCountdownNumber2);
            viewHolder.txtCountdownText2 = (TextView) view.findViewById(R.id.txtCountdownText2);
            viewHolder.txtCountdownNumber3 = (TextView) view.findViewById(R.id.txtCountdownNumber3);
            viewHolder.txtCountdownText3 = (TextView) view.findViewById(R.id.txtCountdownText3);

            viewHolder.layoutTimeActual = (LinearLayout) view.findViewById(R.id.layoutTimeActual);
            viewHolder.txtYearStart = (TextView) view.findViewById(R.id.txtYearStart);
            viewHolder.txtYearEnd = (TextView) view.findViewById(R.id.txtYearEnd);
            viewHolder.layoutSingleDate = (LinearLayout) view.findViewById(R.id.layoutSingleDate);
            viewHolder.txtDate = (TextView) view.findViewById(R.id.txtDate);
            viewHolder.txtMonth = (TextView) view.findViewById(R.id.txtMonth);
            viewHolder.layoutDoubleDate = (LinearLayout) view.findViewById(R.id.layoutDoubleDate);
            viewHolder.txtDateStart = (TextView) view.findViewById(R.id.txtDateStart);
            viewHolder.txtDateEnd = (TextView) view.findViewById(R.id.txtDateEnd);
            viewHolder.txtTimeStart = (TextView) view.findViewById(R.id.txtTimeStart);
            viewHolder.txtTimeEnd = (TextView) view.findViewById(R.id.txtTimeEnd);

            viewHolder.txtHoverNote = (EditText) view.findViewById(R.id.editNote);
            viewHolder.layoutLocation = (RelativeLayout) view.findViewById(R.id.layoutLocation);
            viewHolder.txtHoverLocation = (TextView) view.findViewById(R.id.txtLocation);

            viewHolder.icLike = (ImageView) view.findViewById(R.id.icLike);
            viewHolder.icComment = (ImageView) view.findViewById(R.id.icComment);
            viewHolder.icAddPhoto = (ImageView) view.findViewById(R.id.icAddPhoto);
            viewHolder.icOptions = (ImageView) view.findViewById(R.id.icOptions);

            viewHolder.layoutBtnLikes = (LinearLayout) view.findViewById(R.id.layoutBtnLikes);
            viewHolder.txtLikesCount = (TextView) view.findViewById(R.id.txtLikeCount);
            viewHolder.layoutBtnComments = (LinearLayout) view.findViewById(R.id.layoutBtnComments);
            viewHolder.txtCommentsCount = (TextView) view.findViewById(R.id.txtCommentsCount);

            viewHolder.txtDescription = (TextView) view.findViewById(R.id.txtDescription);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        // Populate the ViewHolder
        final int index = i;
        final STXPost post = mListPosts.get(index);

        // Set profile image
        if (post.user().profilePictureURL != null && !post.user().profilePictureURL.isEmpty()) {
            Picasso.with(mContext).load(Constants.BASE_URL + post.user().profilePictureURL).fit().transform(transformationCirlce).into(viewHolder.imgUserProfile);
        }

        // Set main image
        final ProgressBar progressBar = viewHolder.progressBar;
        progressBar.setVisibility(View.VISIBLE);
        if (post.link != null && !post.link.isEmpty()) {
            Picasso.with(mContext).load(Constants.BASE_URL + post.link).into(viewHolder.imgMain, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

        // Set username and image title
        viewHolder.txtUsername.setText(post.user().username);
        viewHolder.txtImgTitle.setText(post.TNName);

        // Set Post Share Type
        String strShareType = "";
        switch (post.share_type) {
            case "1":
                strShareType = mContext.getString(R.string.share_type_public);
                break;
            case "2":
                strShareType = mContext.getString(R.string.share_type_followers);
                break;
            case "3":
                strShareType = mContext.getString(R.string.share_type_only_me);
                break;
            case "4":
                strShareType = mContext.getString(R.string.share_type_custom_private);
                break;
            default:
        }
        viewHolder.txtPostShareType.setText(strShareType);

        // Set Confidential Note Status
        if (viewHolder.txtHoverNote.getText().toString().isEmpty()) {
            viewHolder.icNoteStatus.setImageResource(R.drawable.shape_circle_note_off);
        } else {
            viewHolder.icNoteStatus.setImageResource(R.drawable.shape_circle_note_on);
        }

        // Set ClickListener for Time View
        viewHolder.layoutCountDownTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setmListTimeViewState(index, 1);
            }
        });

        viewHolder.layoutTimeActual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setmListTimeViewState(index, 0);
            }
        });

        // Set Countdown Timer
        viewHolder = updateDecompte(mContext, post, viewHolder);

        // Set Actual Time
        viewHolder = updateDateFormat(mContext, post, viewHolder);

        // Set Time View
        if (getmListTimeViewState(index) == 0) {
            viewHolder.layoutTimeActual.setVisibility(View.GONE);
            viewHolder.layoutCountDownTimer.setVisibility(View.VISIBLE);
        } else if (getmListTimeViewState(index) == 1) {
            viewHolder.layoutCountDownTimer.setVisibility(View.GONE);
            viewHolder.layoutTimeActual.setVisibility(View.VISIBLE);
        }

        // Set icons

        // More photos icon
        if (post.photos_count.equals("1")) {
            viewHolder.icMorePhotos.setVisibility(View.GONE);
        } else {
            viewHolder.icMorePhotos.setVisibility(View.VISIBLE);
        }

        // Reminder icon
        if (post.reminded.equals("1")) {
            viewHolder.icReminderBell.setImageResource(R.drawable.ic_reminder_bell_on);
        } else {
            viewHolder.icReminderBell.setImageResource(R.drawable.ic_reminder_bell_off);
        }

        // Likes icon

        // Comments icon

        // Add photo icon
        if (post.add_photo_type.equals("1")) {
            viewHolder.icAddPhoto.setVisibility(View.VISIBLE);
        } else {
            viewHolder.icAddPhoto.setVisibility(View.GONE);
        }

        // Update Likes and Comments count
        viewHolder.txtLikesCount.setText(post.likes + " " + mContext.getString(R.string.likes));
        viewHolder.txtCommentsCount.setText(post.totalComments() + " " + (post.totalComments() != 1 ? mContext.getString(R.string.comments) : mContext.getString(R.string.comment)));

        // Add delegate functions for Likes and Comments
        viewHolder.layoutBtnLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Open new activity. Pass <post> object in bundle and use it in new activity
                Toast.makeText(mContext, "Likes clicked for post item = " + index, Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.layoutBtnComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Open new activity. Pass <post> object in bundle and use it in new activity
                Toast.makeText(mContext, "Comments clicked for post item = " + index, Toast.LENGTH_SHORT).show();
            }
        });


        // Update description
        viewHolder.txtDescription.setText(post.captionText());

        // Set Location
        if (post.location_infos.isEmpty()) {
            viewHolder.txtHoverLocation.setText(mContext.getString(R.string.no_location));
        } else {
            try {
                JSONObject objLocation = new JSONObject(post.location_infos);
                JSONArray arrayAddressJSON = objLocation.getJSONArray("FormattedAddressLines");
                ArrayList<String> arrayAddress = new ArrayList<String>();
                for (int j = 0; j < arrayAddressJSON.length(); j++) {
                    arrayAddress.add(arrayAddressJSON.getString(j));
                }
                viewHolder.txtHoverLocation.setText(arrayAddress.toString().replace("[", "").replace("]", ""));
            } catch (Exception e) {
                e.printStackTrace();
                viewHolder.txtHoverLocation.setText(mContext.getString(R.string.no_location));
            }

        }

        // Set HoverLayout on BlurLayout based on SwipeState
        if (mListItemSwipeState.get(i) != null) {
            switch (mListItemSwipeState.get(i)) {
                case -1:
                    viewHolder.imgBlur.setImageBitmap(BlurBuilder.blur(viewHolder.layoutImgMainBase));
                    viewHolder.imgBlur.setVisibility(View.VISIBLE);
                    viewHolder.layoutLocation.setVisibility(View.GONE);
                    if (viewHolder.txtHoverNote.getVisibility() == View.GONE) {
                        viewHolder.txtHoverNote.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.FadeInLeft)
                                .duration(300)
                                .playOn(viewHolder.txtHoverNote);
                    }
                    break;

                case 0:
                    if (viewHolder.txtHoverNote.getVisibility() == View.VISIBLE) {
                        YoYo.with(Techniques.FadeOutLeft)
                                .duration(300)
                                .playOn(viewHolder.txtHoverNote);
                        viewHolder.txtHoverNote.setVisibility(View.GONE);
                    }

                    if (viewHolder.layoutLocation.getVisibility() == View.VISIBLE) {
                        YoYo.with(Techniques.FadeOutRight)
                                .duration(300)
                                .playOn(viewHolder.layoutLocation);
                        viewHolder.layoutLocation.setVisibility(View.GONE);
                    }
                    viewHolder.imgBlur.setVisibility(View.GONE);

                    break;

                case 1:
                    viewHolder.imgBlur.setImageBitmap(BlurBuilder.blur(viewHolder.layoutImgMainBase));
                    viewHolder.imgBlur.setVisibility(View.VISIBLE);
                    viewHolder.txtHoverNote.setVisibility(View.GONE);
                    if (viewHolder.layoutLocation.getVisibility() == View.GONE) {
                        viewHolder.layoutLocation.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.FadeInRight)
                                .duration(300)
                                .playOn(viewHolder.layoutLocation);
                    }
                    break;
            }
        } else {
            viewHolder.imgBlur.setVisibility(View.GONE);
            viewHolder.txtHoverNote.setVisibility(View.GONE);
            viewHolder.layoutLocation.setVisibility(View.GONE);
        }

        return view;
    }

    private ViewHolder updateDateFormat(Context context, STXPost postItem, ViewHolder viewHolder) {

        long timestamp = (long) (Double.parseDouble(postItem.time) * 1000);
        Calendar postDate = Calendar.getInstance();
        postDate.setTimeInMillis(timestamp);

        int year = postDate.get(Calendar.YEAR);
        int date = postDate.get(Calendar.DAY_OF_MONTH);
        int month = postDate.get(Calendar.MONTH);
        String monthName = postDate.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
        int hour = postDate.get(Calendar.HOUR_OF_DAY);
        int min = postDate.get(Calendar.MINUTE);

        if (postItem.time_end.isEmpty()) {

            viewHolder.txtYearStart.setText(Integer.toString(year));
            viewHolder.txtYearStart.setVisibility(View.VISIBLE);
            viewHolder.txtYearEnd.setVisibility(View.GONE);

            viewHolder.txtDate.setText(Integer.toString(date));
            viewHolder.txtMonth.setText(monthName);
            viewHolder.layoutDoubleDate.setVisibility(View.GONE);
            viewHolder.layoutSingleDate.setVisibility(View.VISIBLE);

            viewHolder.txtTimeStart.setText(String.format("%02d:%02d", hour, min));
            viewHolder.txtTimeStart.setVisibility(View.VISIBLE);
            viewHolder.txtTimeEnd.setVisibility(View.GONE);

        } else {

            long endTimestamp = (long) (Double.parseDouble(postItem.time_end) * 1000);
            Calendar postEndDate = Calendar.getInstance();
            postEndDate.setTimeInMillis(endTimestamp);

            int yearEnd = postEndDate.get(Calendar.YEAR);
            int dateEnd = postEndDate.get(Calendar.DAY_OF_MONTH);
            int monthEnd = postEndDate.get(Calendar.MONTH);
            String monthNameEnd = postEndDate.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US);
            int hourEnd = postEndDate.get(Calendar.HOUR_OF_DAY);
            int minEnd = postEndDate.get(Calendar.MINUTE);

            if (postDate.compareTo(postEndDate) == 0) {

                viewHolder.txtYearStart.setText(Integer.toString(year));
                viewHolder.txtYearStart.setVisibility(View.VISIBLE);
                viewHolder.txtYearEnd.setVisibility(View.GONE);

                viewHolder.txtDate.setText(Integer.toString(date));
                viewHolder.txtMonth.setText(monthName);
                viewHolder.layoutDoubleDate.setVisibility(View.GONE);
                viewHolder.layoutSingleDate.setVisibility(View.VISIBLE);

                viewHolder.txtTimeStart.setText(String.format("%02d:%02d", hour, min));
                viewHolder.txtTimeStart.setVisibility(View.VISIBLE);
                viewHolder.txtTimeEnd.setVisibility(View.GONE);

            } else {

                if (postDate.compareTo(postEndDate) < 0) {

                    if (year == yearEnd) {
                        viewHolder.txtYearStart.setText(Integer.toString(year));
                        viewHolder.txtYearStart.setVisibility(View.VISIBLE);
                        viewHolder.txtYearEnd.setVisibility(View.GONE);
                    } else {
                        viewHolder.txtYearStart.setText(Integer.toString(year));
                        viewHolder.txtYearEnd.setText(Integer.toString(yearEnd));
                        viewHolder.txtYearStart.setVisibility(View.VISIBLE);
                        viewHolder.txtYearEnd.setVisibility(View.VISIBLE);
                    }

                    if (date == dateEnd && month == monthEnd) {
                        viewHolder.txtDate.setText(Integer.toString(date));
                        viewHolder.txtMonth.setText(monthName);
                        viewHolder.layoutDoubleDate.setVisibility(View.GONE);
                        viewHolder.layoutSingleDate.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.txtDateStart.setText(Integer.toString(date) + " " + monthName);
                        viewHolder.txtDateEnd.setText(Integer.toString(dateEnd) + " " + monthNameEnd);
                        viewHolder.layoutDoubleDate.setVisibility(View.VISIBLE);
                        viewHolder.layoutSingleDate.setVisibility(View.GONE);
                    }

                    if (hour == hourEnd && min == minEnd) {
                        viewHolder.txtTimeStart.setText(String.format("%02d:%02d", hour, min));
                        viewHolder.txtTimeStart.setVisibility(View.VISIBLE);
                        viewHolder.txtTimeEnd.setVisibility(View.GONE);
                    } else {
                        viewHolder.txtTimeStart.setText(String.format("%02d:%02d", hour, min));
                        viewHolder.txtTimeEnd.setText(String.format("%02d:%02d", hourEnd, minEnd));
                        viewHolder.txtTimeStart.setVisibility(View.VISIBLE);
                        viewHolder.txtTimeEnd.setVisibility(View.VISIBLE);
                    }

                } else {

                    if (year == yearEnd) {
                        viewHolder.txtYearStart.setText(Integer.toString(year));
                        viewHolder.txtYearStart.setVisibility(View.VISIBLE);
                        viewHolder.txtYearEnd.setVisibility(View.GONE);
                    } else {
                        viewHolder.txtYearStart.setText(Integer.toString(yearEnd));
                        viewHolder.txtYearEnd.setText(Integer.toString(year));
                        viewHolder.txtYearStart.setVisibility(View.VISIBLE);
                        viewHolder.txtYearEnd.setVisibility(View.VISIBLE);
                    }

                    if (date == dateEnd && month == monthEnd) {
                        viewHolder.txtDate.setText(Integer.toString(date));
                        viewHolder.txtMonth.setText(monthName);
                        viewHolder.layoutDoubleDate.setVisibility(View.GONE);
                        viewHolder.layoutSingleDate.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.txtDateStart.setText(Integer.toString(dateEnd) + " " + monthNameEnd);
                        viewHolder.txtDateEnd.setText(Integer.toString(date) + " " + monthName);
                        viewHolder.layoutDoubleDate.setVisibility(View.VISIBLE);
                        viewHolder.layoutSingleDate.setVisibility(View.GONE);
                    }

                    if (hour == hourEnd && min == minEnd) {
                        viewHolder.txtTimeStart.setText(String.format("%02d:%02d", hour, min));
                        viewHolder.txtTimeStart.setVisibility(View.VISIBLE);
                        viewHolder.txtTimeEnd.setVisibility(View.GONE);
                    } else {
                        viewHolder.txtTimeStart.setText(String.format("%02d:%02d", hourEnd, minEnd));
                        viewHolder.txtTimeEnd.setText(String.format("%02d:%02d", hour, min));
                        viewHolder.txtTimeStart.setVisibility(View.VISIBLE);
                        viewHolder.txtTimeEnd.setVisibility(View.VISIBLE);
                    }

                }
            }

        }

        return viewHolder;
    }

    private ViewHolder updateDecompte(Context context, STXPost postItem, ViewHolder viewHolder) {

        ArrayList<Object> tn_time = postItem.Timestamp;

        if (tn_time.size() > 1) {

            viewHolder.txtCountdownText1.setText(((String[]) tn_time.get(1))[1]);
            viewHolder.txtCountdownText2.setText(((String[]) tn_time.get(2))[1]);
            viewHolder.txtCountdownText3.setText(((String[]) tn_time.get(3))[1]);

            viewHolder.txtCountdownNumber1.setText(((String[]) tn_time.get(1))[0]);
            viewHolder.txtCountdownNumber2.setText(((String[]) tn_time.get(2))[0]);
            viewHolder.txtCountdownNumber3.setText(((String[]) tn_time.get(3))[0]);

            String last_value = (((String[]) tn_time.get(3))[1]);

            if ((((String[]) tn_time.get(1))[0]).equals("00")) {
                viewHolder.txtCountdownText1.setTextColor(Color.LTGRAY);
                viewHolder.txtCountdownNumber1.setTextColor(Color.LTGRAY);
            } else {

                if (postItem.futur()) {
                    viewHolder.txtCountdownText1.setTextColor(Color.WHITE);
                    viewHolder.txtCountdownNumber1.setTextColor(Color.WHITE);
                } else {
                    viewHolder.txtCountdownText1.setTextColor(ContextCompat.getColor(context, R.color.timeFuture));
                    viewHolder.txtCountdownNumber1.setTextColor(ContextCompat.getColor(context, R.color.timeFuture));
                }

            }

            if ((((String[]) tn_time.get(2))[0]).equals("00")) {
                viewHolder.txtCountdownText2.setTextColor(Color.LTGRAY);
                viewHolder.txtCountdownNumber2.setTextColor(Color.LTGRAY);
            } else {

                if (postItem.futur()) {
                    viewHolder.txtCountdownText2.setTextColor(Color.WHITE);
                    viewHolder.txtCountdownNumber2.setTextColor(Color.WHITE);
                } else {
                    viewHolder.txtCountdownText2.setTextColor(ContextCompat.getColor(context, R.color.timeFuture));
                    viewHolder.txtCountdownNumber2.setTextColor(ContextCompat.getColor(context, R.color.timeFuture));
                }

            }

            if ((((String[]) tn_time.get(3))[0]).equals("00")) {
                viewHolder.txtCountdownText3.setTextColor(Color.LTGRAY);
                viewHolder.txtCountdownNumber3.setTextColor(Color.LTGRAY);
            } else {

                if (postItem.futur()) {
                    viewHolder.txtCountdownText3.setTextColor(Color.WHITE);
                    viewHolder.txtCountdownNumber3.setTextColor(Color.WHITE);
                } else {
                    viewHolder.txtCountdownText3.setTextColor(ContextCompat.getColor(context, R.color.timeFuture));
                    viewHolder.txtCountdownNumber3.setTextColor(ContextCompat.getColor(context, R.color.timeFuture));
                }

            }

//            double Timestamp_val = (double)tn_time.get(5);

            if (postItem.futur()) {
                viewHolder.txtCountdownIndicator.setText(context.getString(R.string.since));
                viewHolder.txtCountdownIndicator.setTextColor(ContextCompat.getColor(context, R.color.timePast));
//                viewHolder.progression_view.hidden = YES;
            } else {
//                double current_timestamp = [[NSDate date] timeIntervalSince1970];
//                viewHolder.progression_view.hidden = NO;

//                double denominateur = (-[_postItem.postDate timeIntervalSince1970]) + Timestamp_val;
//                if (denominateur != 0) {
//
//                    viewHolder.progression_width.constant = DEVICE_WIDTH * ((current_timestamp - [_postItem.postDate timeIntervalSince1970])/(denominateur));
//
//                } else {
//
//                    viewHolder.progression_view.hidden = YES;
//
//                }

                viewHolder.txtCountdownIndicator.setText(context.getString(R.string.start_in));
                viewHolder.txtCountdownIndicator.setTextColor(ContextCompat.getColor(context, R.color.timeFuture));

//                viewHolder.progression_view.backgroundColor = CouleurRougeFluo;
            }

        }

        return viewHolder;
    }

    private static class ViewHolder {

        ImageView imgUserProfile;
        TextView txtUsername;
        TextView txtPostShareType;

        RelativeLayout layoutImgMain;
        RelativeLayout layoutImgMainBase;
        ProgressBar progressBar;
        ImageView imgMain;
        TextView txtImgTitle;
        ImageView icMorePhotos;
        ImageView icReminderBell;

        ImageView icNoteStatus;

        LinearLayout layoutCountDownTimer;
        TextView txtCountdownIndicator;
        TextView txtCountdownNumber1;
        TextView txtCountdownText1;
        TextView txtCountdownNumber2;
        TextView txtCountdownText2;
        TextView txtCountdownNumber3;
        TextView txtCountdownText3;

        LinearLayout layoutTimeActual;
        TextView txtYearStart;
        TextView txtYearEnd;
        LinearLayout layoutSingleDate;
        TextView txtDate;
        TextView txtMonth;
        LinearLayout layoutDoubleDate;
        TextView txtDateStart;
        TextView txtDateEnd;
        TextView txtTimeStart;
        TextView txtTimeEnd;

        ImageView imgBlur;
        EditText txtHoverNote;
        RelativeLayout layoutLocation;
        TextView txtHoverLocation;

        ImageView icLike;
        ImageView icComment;
        ImageView icAddPhoto;
        ImageView icOptions;

        LinearLayout layoutBtnLikes;
        TextView txtLikesCount;
        LinearLayout layoutBtnComments;
        TextView txtCommentsCount;

        TextView txtDescription;
    }
}
