package com.pardypanda.timenote;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pardypanda.timenote.api.API_TimeNote;
import com.pardypanda.timenote.api.API_TimeNote.FailedBlock;
import com.pardypanda.timenote.api.API_TimeNote.SuccessfulBlock;
import com.pardypanda.timenote.api.STXPost;
import com.pardypanda.timenote.util.SlidableGesture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class TimelineActivity extends AppCompatActivity {

    private final String TAG = "TimelineActivity";
    public API_TimeNote shared;
    private List<STXPost> mListPosts = new ArrayList<STXPost>();
    private ListView mListTimeLine;
    private boolean mIsListScrolling = false;

    private TimelineAdapter mAdapter;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        setupUI();

        initTimeNote();

        populateList();
    }

    private void setupUI() {
        mListTimeLine = (ListView) findViewById(R.id.listTimeline);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void initTimeNote() {
        // Init API_TimeNote
        shared = API_TimeNote.sharedInstance(this);
    }

    private void populateList() {

        // Load list from cache if available
        String cache = PreferenceManager.getDefaultSharedPreferences(TimelineActivity.this).getString("response", "");
        if (!cache.isEmpty()) {
            populateList(cache, true);
        }

        // Then get data from server
        shared.getTimelineWithPage(0, "", "", null, new SuccessfulBlock() {
            @Override
            public void success(JSONObject response) throws JSONException {
                populateList(response.toString(), false);
            }

        }, new FailedBlock() {
            @Override
            public void failed(Object response) {
                Log.d(TAG, "Error = " + response);
                Toast.makeText(TimelineActivity.this, "Server fetch failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void populateList(String responseString, boolean fromCache) {

        try {

            if (!fromCache) {
                // Update cache with latest data
                PreferenceManager.getDefaultSharedPreferences(TimelineActivity.this).edit().putString("response", responseString).apply();
            }

            JSONObject response = new JSONObject(responseString);

            Log.d(TAG, "Response " + response);

            boolean status = response.getBoolean("success");
            JSONObject data = response.getJSONObject("data");

            if (status) {

                if (data.has("timeline")) {
                    JSONArray jsonTimeline = data.getJSONArray("timeline");

                    for (int i = 0; i < jsonTimeline.length(); i++) {

                        JSONObject jsonPost = jsonTimeline.getJSONObject(i);

                        ArrayList<Object> arrayTimestamp;
                        if (jsonPost.has("time")) {
                            arrayTimestamp = shared.getDetailedTimestamp(jsonPost.getString("time"), false, 0);
                        } else {
                            arrayTimestamp = new ArrayList<Object>();
                            arrayTimestamp.add("+");
                        }

                        jsonPost.put("timestamp", arrayTimestamp);

                        STXPost.sharedInstance().initWithDictionary(jsonPost);

                        mListPosts.add(new STXPost(jsonPost));
                    }

                    if (mListPosts.size() > 0) {
                        if (mAdapter == null) {
                            mAdapter = new TimelineAdapter(TimelineActivity.this, mListPosts);

                            mListTimeLine.setAdapter(mAdapter);

                            mListTimeLine.setOnScrollListener(new AbsListView.OnScrollListener() {
                                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    // TODO Auto-generated method stub
                                }

                                public void onScrollStateChanged(AbsListView view, int scrollState) {
                                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                                        mIsListScrolling = false;
                                    } else {
                                        mIsListScrolling = true;
                                    }
                                }
                            });

                            mListTimeLine.setOnTouchListener(new SlidableGesture(TimelineActivity.this, mListTimeLine) {
                                @Override
                                protected void slideToLeft(int position) {
                                    Log.d(TAG, "Left on " + position + " | Scrolling state = " + mIsListScrolling);

                                    if (!mIsListScrolling) {
                                        if (mAdapter.getSwipeState(position) == 0) {
                                            mAdapter.setSwipeState(position, 1);
                                        } else if (mAdapter.getSwipeState(position) == -1) {
                                            mAdapter.setSwipeState(position, 0);
                                        }
                                    }
                                    super.slideToLeft(position);
                                }

                                @Override
                                protected void slideToRight(int position) {
                                    Log.d(TAG, "Right on " + position + " | Scrolling state = " + mIsListScrolling);

                                    if (!mIsListScrolling) {
                                        if (mAdapter.getSwipeState(position) == 0) {
                                            mAdapter.setSwipeState(position, -1);
                                        } else if (mAdapter.getSwipeState(position) == 1) {
                                            mAdapter.setSwipeState(position, 0);
                                        }
                                    }
                                    super.slideToRight(position);
                                }
                            });

                        } else {
                            mAdapter.updateList(mListPosts);
                        }
                    }
                }


            } else {
                System.out.println("Error " + data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_timeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
