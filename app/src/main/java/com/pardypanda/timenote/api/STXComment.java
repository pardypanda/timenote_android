package com.pardypanda.timenote.api;

import org.json.JSONException;
import org.json.JSONObject;

public class STXComment {
	public String commentID;
	public String text;
	public String postDate;
	public String photo_link;
	public String photo_id;
	public JSONObject fromDictionary;
	
	private static STXComment sharedInstance = null;
	
	public static STXComment sharedInstance()
	{
		if (sharedInstance == null)
		{
			sharedInstance = new STXComment();
		}
		
		return sharedInstance;
	}
	
	public void initWithDictionary(JSONObject dictionary)
	{
		if (sharedInstance == null)
		{
			sharedInstance = new STXComment();
		}
		
		try {
			sharedInstance.commentID = dictionary.getString("id");
			sharedInstance.text = dictionary.getString("text");
			sharedInstance.photo_link = dictionary.getString("photo_link");
			sharedInstance.photo_id = dictionary.getString("photo_id");
			sharedInstance.postDate = dictionary.getString("created_time");
			sharedInstance.fromDictionary = dictionary.getJSONObject("from");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int hash()
	{
		return sharedInstance.commentID.hashCode();
	}
	
	public Boolean isEqualToComment(STXComment comment)
	{
		return comment.commentID.equals(sharedInstance.commentID);
	}
	
	public void updateText(String text)
	{
		sharedInstance.text = text;
	}
	
	public Boolean isEqual(STXComment comment)
	{
		if (this == comment)
			return true;
		
		return isEqualToComment(comment);
	}
	
	public STXUser from()
	{
		STXUser user = STXUser.sharedInstance();
		user.initWithDictionary(sharedInstance.fromDictionary);
		
		return user;
	}
	
	public String description()
	{
		JSONObject dictionary = new JSONObject();
		try {
			if (sharedInstance.commentID != null)
				dictionary.put("commentID", sharedInstance.commentID);
			else
				dictionary.put("commentID", "");
			
			if (sharedInstance.from() != null)
				dictionary.put("from", sharedInstance.from().username);
			else
				dictionary.put("from", "");
			
			if (sharedInstance.text != null)
				dictionary.put("text", sharedInstance.text);
			else
				dictionary.put("text", "");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dictionary.toString();
	}
}
