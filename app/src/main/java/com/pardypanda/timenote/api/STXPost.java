package com.pardypanda.timenote.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class STXPost {

	public String postID;
	public String TNName;
	public String TNDescription;
	public ArrayList<Object> Timestamp;
	public String created_time;
	public String share_type;
	public String add_photo_type;
	public String photos_count;
	public String add_photo;
	public Date postDate;
//	public String nearby;
	public String location_infos;
//	public String distance;
	public String memo_text;

	public float latitude;
	public float longitude;

	public String link;
	public String reminded;
	public String time;
	public String time_end;

	public JSONObject userDictionary;

	public String likes, like_type;
	public ArrayList<STXComment> comments;
	public JSONObject commentsDictionary;
	
	public static final int MAX_NUMBER_OF_COMMENTS = 10; 
	
	private Boolean futur_value;
	
	private static STXPost sharedInstance = null;
	
	public STXPost() {
		
	}
	
	public STXPost(JSONObject dictionary) {
		try {
			ArrayList<Object> timestamp_array = (ArrayList<Object>) dictionary.get("timestamp");
			futur_value = ((String)timestamp_array.get(0)).contains("+");

			postID = dictionary.getString("id");
			time = dictionary.getString("time");
			time_end = dictionary.getString("time_end");
			location_infos = dictionary.getString("location_infos");
			reminded = dictionary.getString("reminded");
			link = dictionary.getString("link");
			share_type = dictionary.getString("share_type");
			add_photo_type = dictionary.getString("allow_add_photo");
			created_time = dictionary.getString("created_time");
			userDictionary = dictionary.getJSONObject("user");
			likes = dictionary.getString("likes");
			memo_text = dictionary.getString("memo_text");
//			nearby = dictionary.getString("nearby");
			latitude = (float) dictionary.getDouble("latitude");
			longitude = (float) dictionary.getDouble("longitude");
//			distance = dictionary.getString("distance");
			photos_count = dictionary.getString("photos_count");
			like_type = dictionary.getString("type");
			TNName = dictionary.getString("name");
			add_photo = dictionary.getString("allow_add_photo");
			TNDescription = dictionary.getString("description");
			Timestamp = timestamp_array;
			commentsDictionary = (JSONObject) dictionary.get("comments");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public static STXPost sharedInstance() {
		if (sharedInstance == null)
		{
			sharedInstance = new STXPost();
		}
		
		return sharedInstance;
	}
	
	public void initWithDictionary(JSONObject dictionary)
	{
		if (sharedInstance == null)
		{
			sharedInstance = new STXPost();
		}
		
		try {
			ArrayList<Object> timestamp_array = (ArrayList<Object>) dictionary.get("timestamp");
			sharedInstance.futur_value = ((String)timestamp_array.get(0)).contains("+");
			
			sharedInstance.postID = dictionary.getString("id");
			sharedInstance.time = dictionary.getString("time");
			sharedInstance.time_end = dictionary.getString("time_end");
			sharedInstance.location_infos = dictionary.getString("location_infos");
			sharedInstance.reminded = dictionary.getString("reminded");
			sharedInstance.link = dictionary.getString("link");
			sharedInstance.share_type = dictionary.getString("share_type");
			sharedInstance.add_photo_type = dictionary.getString("allow_add_photo");
			sharedInstance.created_time = dictionary.getString("created_time");
			sharedInstance.userDictionary = dictionary.getJSONObject("user");
			sharedInstance.likes = dictionary.getString("likes");
			sharedInstance.memo_text = dictionary.getString("memo_text");
//			sharedInstance.nearby = dictionary.getString("nearby");
			sharedInstance.latitude = (float) dictionary.getDouble("latitude");
			sharedInstance.longitude = (float) dictionary.getDouble("longitude");
//			sharedInstance.distance = dictionary.getString("distance");
			sharedInstance.photos_count = dictionary.getString("photos_count");
			sharedInstance.like_type = dictionary.getString("type");
			sharedInstance.TNName = dictionary.getString("name");
			sharedInstance.add_photo = dictionary.getString("allow_add_photo");
			sharedInstance.TNDescription = dictionary.getString("description");
			sharedInstance.Timestamp = timestamp_array;
			sharedInstance.commentsDictionary = (JSONObject) dictionary.get("comments");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int hash()
	{
		return sharedInstance.postID.hashCode();
	}
	
	public Boolean futur()
	{
		return futur_value;
	}
	
	public Number getBrutCommentsNumber() {
		try {
			return (Number) sharedInstance.commentsDictionary.get("count");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Boolean isEqualToPost(STXPost post)
	{
		return post.postID.equals(sharedInstance.postID);
	}
	
	public Boolean isEqual(STXPost post)
	{
		if (this == post)
			return true;
		
		return isEqualToPost(post);
	}
	
	public String description()
	{
		JSONObject dictionary = new JSONObject();
		try {
			if (sharedInstance.postID != null)
				dictionary.put("postID", sharedInstance.postID);
			else
				dictionary.put("postID", "");
			
			if (sharedInstance.postDate != null)
				dictionary.put("postDate", sharedInstance.postDate);
			else
				dictionary.put("postDate", "");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dictionary.toString();
	}
	
	public Date postDate()
	{
		long createdTime = (long)Double.parseDouble(sharedInstance.created_time);
		Date createdDate = new Date(createdTime);
		
		return createdDate;
	}
	
	public void setLikesValue(String likes_value) {
		sharedInstance.likes = likes_value;
	}
	
	public String captionText() {
		return sharedInstance.TNDescription;
	}
	
	public void setReminder(String reminded) {
		sharedInstance.reminded = reminded;
	}
	
	public void updateTimestamp(ArrayList<Object> array) {
		sharedInstance.Timestamp = array;
		futur_value = ((String)array.get(0)).contains("+");
	}
	
	public void updatePost(JSONObject dico)
	{
		try {
			ArrayList<Object> timestamp_array = (ArrayList<Object>) dico.get("timestamp");
			futur_value = ((String)timestamp_array.get(0)).contains("+");
			
			sharedInstance.time = dico.getString("time");
			sharedInstance.time_end = dico.getString("time_end");
			sharedInstance.location_infos = dico.getString("location_infos");
			sharedInstance.link = dico.getString("link");
			sharedInstance.share_type = dico.getString("share_type");
			sharedInstance.add_photo_type = dico.getString("allow_add_photo");
			sharedInstance.TNName = dico.getString("name");
			sharedInstance.photos_count = dico.getString("photos_count");
			sharedInstance.TNDescription = dico.getString("description");
			sharedInstance.Timestamp = timestamp_array;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<STXComment> comments()
	{
		if (sharedInstance.comments == null)
		{
			ArrayList<STXComment> mutableComments = new ArrayList<STXComment>();
			try {
				ArrayList<JSONObject> commentsArray = (ArrayList<JSONObject>) sharedInstance.commentsDictionary.get("data");
				for (int i = 0; i < commentsArray.size(); i++)
				{
					JSONObject commentDictionary = commentsArray.get(i);
					STXComment comment = STXComment.sharedInstance();
					comment.initWithDictionary(commentDictionary);
					mutableComments.add(comment);
				}
				
				sharedInstance.comments = mutableComments;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return sharedInstance.comments;
	}
	
	public void setType(String type)
	{
		sharedInstance.like_type = type;
	}
	
	public void setMemo(String memo)
	{
		sharedInstance.memo_text = memo;
	}
	
	public int totalComments()
	{
		try {
			int count = Integer.parseInt(sharedInstance.commentsDictionary.getString("count"));
			JSONArray dataArray = sharedInstance.commentsDictionary.getJSONArray("data");
			int count_coms = dataArray.length();
			if (count_coms < MAX_NUMBER_OF_COMMENTS || (count > 0 && count_coms == 0)) {
				return count_coms;
			}
			
			return count;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public int BrutalTotalComments()
	{
		try {
			Number count = (Number) sharedInstance.commentsDictionary.get("count");
			
			return count.intValue();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public STXUser user()
	{
//		STXUser user = STXUser.sharedInstance();
//		user.initWithDictionary(sharedInstance.userDictionary);

		STXUser user = new STXUser(userDictionary);
		
		return user;
	}
	
}
