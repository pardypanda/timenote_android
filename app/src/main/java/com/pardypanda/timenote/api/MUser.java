package com.pardypanda.timenote.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MUser {
	private static MUser sharedInstance = null;
	private static Context applicationContext;
	private static final String APP_NAME = "timenote";
	
	public String userId;
	public String token;
	public String description_user;
	public String email;
	public String fullName;
	public String avatar;
	public String username;
	public String timers_count;
	public String follow_count;
	public String followers_count;
	public String facebookID;
	public String facebookToken;
	public String birthday;
	public String from_location;
	public String website;
	public String phoneNumber;
	public String certified;
	public String pushID;
	public String pushToken;
	public String ReceiveNotifications;
	public String PrivateMode;
	
	public static void init(Context c){
		applicationContext = c;
	}
	
	public static MUser currentUser()
	{
		if (sharedInstance != null)
			return sharedInstance;
		
		sharedInstance = new MUser();
		
		SharedPreferences prefs = applicationContext.getSharedPreferences(APP_NAME,
				Context.MODE_PRIVATE);
		
		sharedInstance.userId = prefs.getString("CURRENT_USER_ID", null);
		sharedInstance.email = prefs.getString("CURRENT_USER_EMAIL", null);
		sharedInstance.avatar = prefs.getString("CURRENT_USER_AVATAR", null);
		sharedInstance.username = prefs.getString("CURRENT_USER_USERNAME", null);
		sharedInstance.token = prefs.getString("CURRENT_USER_TOKEN", null);
		sharedInstance.fullName = prefs.getString("CURRENT_USER_FULLNAME", null);
		sharedInstance.facebookID = prefs.getString("CURRENT_USER_FB_ID", null);
		sharedInstance.birthday = prefs.getString("CURRENT_USER_BIRTHDAY", null);
		sharedInstance.from_location = prefs.getString("CURRENT_USER_FROM_LOCATION", null);
		sharedInstance.website = prefs.getString("CURRENT_USER_WEBSITE", null);
		sharedInstance.facebookToken = prefs.getString("CURRENT_USER_FB_TOKEN", null);
		sharedInstance.phoneNumber = prefs.getString("CURRENT_USER_PHONE_NUMBER", null);
		sharedInstance.description_user = prefs.getString("CURRENT_USER_DESCRIPTION", null);
		sharedInstance.timers_count = prefs.getString("CURRENT_USER_TIMER_COUNT", null);
		sharedInstance.follow_count = prefs.getString("CURRENT_USER_FOLLOW_COUNT", null);
		sharedInstance.followers_count = prefs.getString("CURRENT_USER_FOLLOWERS_COUNT", null);
		sharedInstance.ReceiveNotifications = prefs.getString("CURRENT_USER_RECEIVE_NOTIFICATIONS", null);
		sharedInstance.pushID = prefs.getString("CURRENT_USER_PUSHID", null);
		sharedInstance.pushToken = prefs.getString("CURRENT_USER_PUSHTOKEN", null);
		sharedInstance.certified = prefs.getString("CURRENT_USER_CERTIFIED", null);
		sharedInstance.PrivateMode = prefs.getString("CURRENT_USER_PRIVATE", null);
		
		return sharedInstance;
	}
	
	public static void saveCurrentUser()
	{
		SharedPreferences prefs = applicationContext.getSharedPreferences(APP_NAME,
				Context.MODE_PRIVATE);
		Editor edit = prefs.edit();
		
		if (MUser.currentUser().userId != null)
			edit.putString("CURRENT_USER_ID", MUser.currentUser().userId);
		
		if (MUser.currentUser().email != null)
			edit.putString("CURRENT_USER_EMAIL", MUser.currentUser().email);
		
		if (MUser.currentUser().avatar != null)
			edit.putString("CURRENT_USER_AVATAR", MUser.currentUser().avatar);
		
		if (MUser.currentUser().username != null)
			edit.putString("CURRENT_USER_USERNAME", MUser.currentUser().username);
		
		if (MUser.currentUser().fullName != null)
			edit.putString("CURRENT_USER_FULLNAME", MUser.currentUser().fullName);
		
		if (MUser.currentUser().token != null)
			edit.putString("CURRENT_USER_TOKEN", MUser.currentUser().token);
		
		if (MUser.currentUser().birthday != null)
			edit.putString("CURRENT_USER_BIRTHDAY", MUser.currentUser().birthday);
		
		if (MUser.currentUser().website != null)
			edit.putString("CURRENT_USER_WEBSITE", MUser.currentUser().website);
		
		if (MUser.currentUser().from_location != null)
			edit.putString("CURRENT_USER_FROM_LOCATION", MUser.currentUser().from_location);
		
		if (MUser.currentUser().facebookID != null)
			edit.putString("CURRENT_USER_FB_ID", MUser.currentUser().facebookID);
		
		if (MUser.currentUser().facebookToken != null)
			edit.putString("CURRENT_USER_FB_TOKEN", MUser.currentUser().facebookToken);
		
		if (MUser.currentUser().phoneNumber != null)
			edit.putString("CURRENT_USER_PHONE_NUMBER", MUser.currentUser().phoneNumber);
		
		if (MUser.currentUser().description_user != null)
			edit.putString("CURRENT_USER_DESCRIPTION", MUser.currentUser().description_user);
		
		if (MUser.currentUser().timers_count != null)
			edit.putString("CURRENT_USER_TIMER_COUNT", MUser.currentUser().timers_count);
		
		if (MUser.currentUser().follow_count != null)
			edit.putString("CURRENT_USER_FOLLOW_COUNT", MUser.currentUser().follow_count);
		
		if (MUser.currentUser().followers_count != null)
			edit.putString("CURRENT_USER_FOLLOWERS_COUNT", MUser.currentUser().followers_count);
		
		if (MUser.currentUser().pushID != null)
			edit.putString("CURRENT_USER_PUSHID", MUser.currentUser().pushID);
		
		if (MUser.currentUser().pushToken != null)
			edit.putString("CURRENT_USER_PUSHTOKEN", MUser.currentUser().pushToken);
		
		if (MUser.currentUser().ReceiveNotifications != null)
			edit.putString("CURRENT_USER_RECEIVE_NOTIFICATIONS", MUser.currentUser().ReceiveNotifications);
		
		if (MUser.currentUser().PrivateMode != null)
			edit.putString("CURRENT_USER_PRIVATE", MUser.currentUser().PrivateMode);
		
		if (MUser.currentUser().certified != null)
			edit.putString("CURRENT_USER_CERTIFIED", MUser.currentUser().certified);
		
		
		edit.commit();	
	}
	
	public static void removeCurrentUser()
	{
		sharedInstance = null;
	    
		SharedPreferences prefs = applicationContext.getSharedPreferences(APP_NAME,
				Context.MODE_PRIVATE);
		
		Editor edit = prefs.edit();
		edit.remove("CURRENT_USER_ID");
		edit.remove("CURRENT_USER_EMAIL");
		edit.remove("CURRENT_USER_AVATAR");
		edit.remove("CURRENT_USER_USERNAME");
		edit.remove("CURRENT_USER_TOKEN");
		edit.remove("CURRENT_USER_FB_ID");
		edit.remove("CURRENT_USER_FULLNAME");
		edit.remove("CURRENT_USER_FB_TOKEN");
		edit.remove("CURRENT_USER_PHONE_NUMBER");
		edit.remove("CURRENT_USER_DESCRIPTION");
		edit.remove("CURRENT_USER_BIRTHDAY");
		edit.remove("CURRENT_USER_WEBSITE");
		edit.remove("CURRENT_USER_FROM_LOCATION");
		edit.remove("CURRENT_USER_TIMER_COUNT");
		edit.remove("CURRENT_USER_FOLLOW_COUNT");
		edit.remove("CURRENT_USER_FOLLOWERS_COUNT");
		edit.remove("CURRENT_USER_PUSHID");
		edit.remove("CURRENT_USER_PUSHTOKEN");
		edit.remove("CURRENT_USER_RECEIVE_NOTIFICATIONS");
		edit.remove("CURRENT_USER_PRIVATE");
		edit.remove("CURRENT_USER_CERTIFIED");
		
		edit.commit();
	}
}
