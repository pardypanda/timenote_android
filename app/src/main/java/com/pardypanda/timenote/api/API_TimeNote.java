package com.pardypanda.timenote.api;

import android.content.Context;
import android.media.Image;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pardypanda.timenote.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class API_TimeNote {

    public static final String BASE_HOST_URL = "http://timenote.go-snapper.com";
    public static Context context;
    private static API_TimeNote sharedInstance = null;

    public static API_TimeNote sharedInstance(Context context_local) {
        if (sharedInstance == null) {
            sharedInstance = new API_TimeNote();
        }

        MUser.init(context_local);
        context = context_local;

        return sharedInstance;
    }

    public static Map<String, String> jsonString2Map(JSONObject jsonObject) throws JSONException {
        Map<String, String> keys = new HashMap<String, String>();

        Iterator<?> keyset = jsonObject.keys(); // HM

        while (keyset.hasNext()) {
            String key = (String) keyset.next();
            Object value = jsonObject.get(key);
            System.out.print("\n Key : " + key);
            if (value instanceof JSONObject) {
                System.out.println("Incomin value is of JSONObject : ");
                keys.put(key, String.valueOf(jsonString2Map((JSONObject) value)));
            }
        }
        return keys;
    }

    private static final String md5(final String password) {
        try {

            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void checkExistedFacebookAccount(String facebookId, String email, String accessToken,
                                            ArrayList<String> pushIds, SuccessfulBlock success, FailedBlock failed) {
        String path = "facebook/check_facebook";
        String push_token;
        String push_id;

        if (pushIds.size() == 2) {
            push_id = pushIds.get(0);
            push_token = pushIds.get(1);

            if (push_id == null || push_id.equals("") || push_id.length() == 0) {
                push_id = "";
            }

            if (push_token == null || push_token.equals("") || push_token.length() == 0) {
                push_token = "";
            }
        } else {
            push_id = "";
            push_token = "";
        }

        Map<String, String> params = new HashMap<String, String>();
        try {
            JSONObject data = new JSONObject();
            data.put("fb_id", facebookId);
            data.put("fb_email", email);
            data.put("fb_token", accessToken);
            data.put("push_id", push_id);
            data.put("push_token", push_token);

            params.put("data", data.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, params, null, success, failed);
    }

    public void performRegisterWithUsername(String userName, String email, String password,
                                            String birthday, ArrayList<String> pushIds, SuccessfulBlock success, FailedBlock failed) {
        String path = "user_push/register";
        String push_token;
        String push_id;

        if (pushIds.size() == 2) {
            push_id = pushIds.get(0);
            push_token = pushIds.get(1);

            if (push_id == null || push_id.equals("") || push_id.length() == 0) {
                push_id = "";
            }

            if (push_token == null || push_token.equals("") || push_token.length() == 0) {
                push_token = "";
            }
        } else {
            push_id = "";
            push_token = "";
        }

        Map<String, String> params = new HashMap<String, String>();
        try {
            JSONObject data = new JSONObject();
            data.put("username", userName);
            data.put("password", password);
            data.put("email", email);
            data.put("push_id", push_id);
            data.put("push_token", push_token);
            data.put("birthday", birthday);

            params.put("data", data.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, params, null, success, failed);

    }

    public void performLoginWithUserName(String userName, String password, ArrayList<String> pushIds,
                                         SuccessfulBlock success, FailedBlock failed) {
        String path = "user/login";
        String push_token;
        String push_id;

        if (pushIds.size() == 2) {
            push_id = pushIds.get(0);
            push_token = pushIds.get(1);

            if (push_id == null || push_id.equals("") || push_id.length() == 0) {
                push_id = "";
            }

            if (push_token == null || push_token.equals("") || push_token.length() == 0) {
                push_token = "";
            }
        } else {
            push_id = "";
            push_token = "";
        }

        Map<String, String> params = new HashMap<String, String>();
        try {
            JSONObject data = new JSONObject();
            data.put("username", userName);
            data.put("password", md5(password));
            data.put("push_id", push_id);
            data.put("push_token", push_token);

            System.out.println("PASSWORD MD5 = " + md5(password));

            params.put("data", data.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, params, null, success, failed);
    }

    public void updateMemoForTimer(String timer_id, String text,
                                   SuccessfulBlock success, FailedBlock failed) {
        String path = "user/update_memo";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("memo", text);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void postCommentOnTimer(String timer_id, String comment,
                                   SuccessfulBlock success, FailedBlock failed) {
        String path = "user_push/post_comment";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("comment", comment);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void loadContactsforUserId(String user_id, String type, int page,
                                      SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_contacts";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("item_id", user_id);
            infos.put("type", type);
            infos.put("page", Integer.toString(page));

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getContactsForVotes(String item_id, String type, int current_page,
                                    SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_contacts_for_votes";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("item_id", item_id);
            infos.put("type", type);
            infos.put("page", Integer.toString(current_page));

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getTimelineWithPage(int page, String filter, String time, String search_text,
                                    SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_timeline";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", "4" /*MUser.currentUser().userId*/);
            infos.put("page", Integer.toString(page));
            infos.put("filter", filter);
            infos.put("time", time);
            infos.put("search", search_text);
            infos.put("free_access", true);
            infos.put("upcoming", "0");
            infos.put("happened", "0");
            infos.put("posted_offset", "0");

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getNearbyWithPage(int page, float distance, float latitude, float longitude,
                                  String filter, String time, String search_text, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_nearby";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("page", Integer.toString(page));
            infos.put("distance", Double.toString(distance));
            infos.put("latitude", Double.toString(latitude));
            infos.put("longitude", Double.toString(longitude));
            infos.put("filter", filter);
            infos.put("time", time);
            infos.put("search", search_text);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void loadUserInformations(String userId, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_user_informations";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("user", userId);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void searchTimersWithString(String search, int page, SuccessfulBlock success,
                                       FailedBlock failed) {
        String path = "user/search_timers";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("search", search);
            infos.put("page", Integer.toString(page));

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void searchUserWithString(String search, int page, int mode,
                                     SuccessfulBlock success, FailedBlock failed) {
        String path = "user/search_users";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("search", search);
            infos.put("page", Integer.toString(page));
            infos.put("mode", Integer.toString(mode));

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void followUser(String user, SuccessfulBlock success, FailedBlock failed) {
        String path = "user_push/follow_user";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("user", user);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void unfollowUser(String user, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/unfollow_user";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("user", user);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void editComment(String comment_id, String text, SuccessfulBlock success,
                            FailedBlock failed) {
        String path = "user/edit_comment";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("comment_id", comment_id);
            infos.put("text", text);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void deletePhoto(String photo_id, String timer_id, SuccessfulBlock success,
                            FailedBlock failed) {
        String path = "user/delete_photo";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("photo_id", photo_id);
            infos.put("timer_id", timer_id);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void deleteComment(String comment_id, String timer_id, SuccessfulBlock success,
                              FailedBlock failed) {
        String path = "user/delete_comment";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("comment_id", comment_id);
            infos.put("timer_id", timer_id);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void reportComment(String comment_id, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/report_comment";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("comment_id", comment_id);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void reportTimer(String timer_id, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/report_timer";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void deleteTimer(String timer_id, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/delete_timer";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getCalendarDatesForUser(String user, int type, double date,
                                        SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_calendar_dates";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("profil_user_id", user);
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("type", Integer.toString(type));
            infos.put("set_date", Double.toString(date));

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getTimersForDate(String date, String user, SuccessfulBlock success,
                                 FailedBlock failed) {
        String path = "user/get_timers_for_date";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("profil_user_id", user);
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("date", date);

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);

    }

    public void loadTimerForUser(String user, int page, String notif, ArrayList<String> pushIds,
                                 int filter, int time, int order, int localized, SuccessfulBlock success,
                                 FailedBlock failed) {
        String path = "user/get_profile";
        String push_token;
        String push_id;

        if (pushIds.size() == 2) {
            push_id = pushIds.get(0);
            push_token = pushIds.get(1);

            if (push_id == null || push_id.equals("") || push_id.length() == 0) {
                push_id = "";
            }

            if (push_token == null || push_token.equals("") || push_token.length() == 0) {
                push_token = "";
            }
        } else {
            push_id = "";
            push_token = "";
        }

        if (notif == null) {
            notif = "";
        }

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("profil_user_id", user);
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("page", Integer.toString(page));
            infos.put("notif", notif);
            infos.put("push_id", push_id);
            infos.put("push_token", push_token);
            infos.put("filter", Integer.toString(filter));
            infos.put("time", Integer.toString(time));
            infos.put("order", Integer.toString(order));
            infos.put("localized", Integer.toString(localized));

            param.put("data", infos.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getTimer(String timer_id, String notif, Boolean edit_mode,
                         SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_timer_infos";

        if (notif == null || notif.length() == 0) {
            notif = "";
        }

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("notif", notif);
            infos.put("edit_mode", Boolean.toString(edit_mode));

            param.put("data", infos.toString());

            Log.d("Timenote", "Notif Get Timer Info = " + infos.toString().replace("\\", ""));

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getSharedForTimer(String timer_id, int page, SuccessfulBlock success,
                                  FailedBlock failed) {
        String path = "user/get_shared_users";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("page", Integer.toString(page));

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void addReminderForTimer(String timer_id, String time, String mode, String type,
                                    String mode_text, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/add_reminder";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("time", time);
            infos.put("mode", mode);
            infos.put("type", type);
            infos.put("mode_text", mode_text);

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void updateReminderForTimer(String timer_id, String reminder_id, String time, String mode,
                                       String type, String mode_text, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/update_reminder";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("time", time);
            infos.put("mode", mode);
            infos.put("reminder_id", reminder_id);
            infos.put("type", type);
            infos.put("mode_text", mode_text);

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getReminderForTimer(String timer_id, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_reminder";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void deleteReminderForTimer(String timer_id, String reminder_id, SuccessfulBlock success,
                                       FailedBlock failed) {
        String path = "user/delete_reminder";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("reminder_id", reminder_id);

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getCommentsForTimer(String timer_id, int page, SuccessfulBlock success,
                                    FailedBlock failed) {
        String path = "user/get_comments";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("timer_id", timer_id);
            infos.put("page", Integer.toString(page));

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void forgotPasswordForUser(String user, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/forgot_password";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("user", user);

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getRequestsSentwithPage(int page, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/get_requests_sent";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("page", Integer.toString(page));

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getFollowRequestwithPage(int page, String notif, SuccessfulBlock success,
                                         FailedBlock failed) {
        String path = "user/get_follow_request";

        if (notif == null || notif.length() == 0) {
            notif = "";
        }

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("page", Integer.toString(page));
            infos.put("notif", notif);

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void performVoteOnRequest(Boolean value, String asker_id, SuccessfulBlock success,
                                     FailedBlock failed) {
        String path = "user_push/private_follow";
        String response = null;

        if (value) {
            response = "accept";
        } else {
            response = "refuse";
        }

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("response", response);
            infos.put("asker_id", asker_id);

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void getNotifications(String type, int page, SuccessfulBlock success,
                                 FailedBlock failed) {
        String path = "user/get_notifications";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            //infos.put("type", type);
            infos.put("page", Integer.toString(page));

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, null, success, failed);
    }

    public void updateTimerWithData(JSONObject data, ArrayList<Image> images, ArrayList<String> index,
                                    int image_count, SuccessfulBlock success, FailedBlock failed) {
        String path = "user/update_timer";

        String total_index = "";
        for (int i = 0; i < index.size(); i++) {
            String one_index = index.get(i);
            total_index += one_index;
            if (i < index.size() - 1) {
                total_index += ",";
            }
        }

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("name", data.getString("name"));
            infos.put("description", data.getString("description"));
            infos.put("share_type", data.getString("share_type"));
            infos.put("allow_add_photo", data.getString("allow_add_photo"));
            infos.put("timestamp", data.getString("timestamp"));
            infos.put("timer_id", data.getString("timer_id"));
            infos.put("new_photos", Integer.toString(image_count));
            infos.put("remove_id", total_index);
            infos.put("shared_array", data.getString("shared_array"));
            infos.put("add_photo_type", data.getString("add_photo_type"));
            infos.put("enable_location", data.getString("enable_location"));
            infos.put("latitude", data.getString("latitude"));
            infos.put("longitude", data.getString("longitude"));
            infos.put("location_infos", data.getString("location_infos"));
            infos.put("timestamp_end", data.getString("timestamp_end"));

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, images, success, failed);
    }

    public void addTimerWithData(JSONObject data, ArrayList<Image> images,
                                 SuccessfulBlock success, FailedBlock failed) {
        String path = "user/add_timer";

        Map<String, String> param = new HashMap<String, String>();
        try {
            JSONObject infos = new JSONObject();
            infos.put("username", MUser.currentUser().username);
            infos.put("access_token", MUser.currentUser().token);
            infos.put("user_id", MUser.currentUser().userId);
            infos.put("name", data.getString("name"));
            infos.put("description", data.getString("description"));
            infos.put("share_type", data.getString("share_type"));
            infos.put("allow_add_photo", data.getString("allow_add_photo"));
            infos.put("timestamp", data.getString("timestamp"));
            infos.put("shared_array", data.getString("shared_array"));
            infos.put("add_photo_type", data.getString("add_photo_type"));
            infos.put("enable_location", data.getString("enable_location"));
            infos.put("latitude", data.getString("latitude"));
            infos.put("longitude", data.getString("longitude"));
            infos.put("location_infos", data.getString("location_infos"));
            infos.put("timestamp_end", data.getString("timestamp_end"));

            param.put("data", infos.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        post(path, param, images, success, failed);
    }

    public double getDiff(String timestamp, double optional) {

        double timer_tt = Double.parseDouble(timestamp);
        double current_tt;

        if (optional != 0) {
            current_tt = optional;
        } else {
            current_tt = (double) (System.currentTimeMillis() / 1000);
        }

        double diff = timer_tt - current_tt;

        return diff;
    }

    public String timestampToText(String timestamp, boolean UTC_optional_timestamp, double optional) {

        double diff = getDiff(timestamp, optional);

        String return_text;

        if (optional != 0) {
            return_text = "";
        } else {
            if (diff > 0) {
                return_text = "T - ";
            } else {
                return_text = "T + ";
                diff = Math.abs(diff);
            }
        }

        int y_number = 0;

        while ((diff - 31556926) >= 0) {
            diff -= 31556926;
            y_number++;
        }

        int m_number = 0;

        while ((diff - 2629743.83) >= 0) {
            diff -= 2629743.83;
            m_number++;
        }

        int day_number = 0;

        while ((diff - 86400) >= 0) {
            diff -= 86400;
            day_number++;
        }

        int hour_number = 0;

        while ((diff - 3600) >= 0) {
            diff -= 3600;
            hour_number++;
        }

        int min_number = 0;

        while ((diff - 60) >= 0) {
            diff -= 60;
            min_number++;
        }

        int sec_number = (int) diff;

        int i = 0;

        if (y_number > 0 && i < 3) {
            return_text = return_text + " " + y_number + "Y";
            i++;
        }

        if (m_number > 0 && i < 3) {
            return_text = return_text + " " + m_number + "M";
            i++;
        }

        if (day_number > 0 && i < 3) {
            return_text = return_text + " " + day_number + "D";
            i++;
        }

        if (hour_number > 0 && i < 3) {
            return_text = return_text + " " + hour_number + "H";
            i++;
        }

        if (min_number > 0 && i < 3) {
            return_text = return_text + " " + min_number + "M";
            i++;
        }

        if (sec_number > 0 && i < 3) {
            return_text = return_text + " " + sec_number + "S";
            i++;
        }

        return return_text;
    }

    public ArrayList<Object> getDetailedTimestamp(String timestamp, boolean UTC_optional_timestamp, double optional) {

        double diff = getDiff(timestamp, optional);

        double initial_diff = diff;

        String years = context.getString(R.string.years);
        String months = context.getString(R.string.months);
        String days = context.getString(R.string.days);
        String hours = context.getString(R.string.hours);
        String mins = context.getString(R.string.mins);
        String secs = context.getString(R.string.secs);

        ArrayList<String[]> order_array = new ArrayList<String[]>();
        order_array.add(new String[]{"", months});
        order_array.add(new String[]{years, days});
        order_array.add(new String[]{months, hours});
        order_array.add(new String[]{days, mins});
        order_array.add(new String[]{hours, secs});
        order_array.add(new String[]{mins, ""});

        ArrayList<Object> return_array = new ArrayList<Object>();

        if (diff > 0) {
            return_array.add("-");
        } else {
            return_array.add("+");
            diff = Math.abs(diff);
        }

        int y_number = 0;

        while ((diff - 31556926) >= 0) {
            diff -= 31556926;
            y_number++;
        }

        int m_number = 0;

        while ((diff - 2629743.83) >= 0) {
            diff -= 2629743.83;
            m_number++;
        }

        int day_number = 0;

        while ((diff - 86400) >= 0) {
            diff -= 86400;
            day_number++;
        }

        int hour_number = 0;

        while ((diff - 3600) >= 0) {
            diff -= 3600;
            hour_number++;
        }

        int min_number = 0;

        while ((diff - 60) >= 0) {
            diff -= 60;
            min_number++;
        }

        int sec_number = (int) diff;

        int i = 0;

        int actual = 0;

        if (y_number > 0 && i < 3) {
            actual = 0;

            if (y_number < 10) {
                return_array.add(new String[]{"0" + y_number, years});
            } else {
                return_array.add(new String[]{"" + y_number, years});
            }
            i++;
        }

        if (m_number > 0 && i < 3) {
            actual = 1;

            if (m_number < 10) {
                return_array.add(new String[]{"0" + m_number, months});
            } else {
                return_array.add(new String[]{"" + m_number, months});
            }
            i++;
        }

        if (day_number > 0 && i < 3) {
            if (day_number < 10) {
                return_array.add(new String[]{"0" + day_number, days});

            } else {
                return_array.add(new String[]{"" + day_number, days});
            }
            actual = 2;
            i++;
        }

        if (hour_number > 0 && i < 3) {
            if (hour_number < 10) {
                return_array.add(new String[]{"0" + hour_number, hours});
            } else {
                return_array.add(new String[]{"" + hour_number, hours});
            }
            actual = 3;
            i++;
        }

        if (min_number > 0 && i < 3) {
            if (min_number < 10) {
                return_array.add(new String[]{"0" + min_number, mins});
            } else {
                return_array.add(new String[]{"" + min_number, mins});
            }
            actual = 4;
            i++;
        }

        if (sec_number > 0 && i < 3) {

            if (sec_number < 10) {
                return_array.add(new String[]{"0" + sec_number, secs});
            } else {
                return_array.add(new String[]{"" + sec_number, secs});
            }
            actual = 5;
            i++;
        }

        int count = return_array.size();

        while (count < 4) {

            if (count == 2) {

                if (actual == 4) {
                    return_array.add(1, new String[] {"00", order_array.get(actual)[0]});
                    return_array.add(3, new String[] {"00", order_array.get(actual)[1]});
                } else if (actual == 5) {
                    return_array.add(1, new String[] {"00", order_array.get(actual)[0]});
                    actual = actual - 1;
                    return_array.add(1, new String[] {"00", order_array.get(actual)[0]});
                } else {
                    return_array.add(new String[] {"00", order_array.get(actual)[1]});
                    actual = actual + 1;
                    return_array.add(new String[] {"00", order_array.get(actual)[1]});
                }

            } else if (count == 3) {

                if (actual == 5) {
                    if (((String[])return_array.get(1))[1].equals(years)) {
                        return_array.add(2, new String[] {"00", months});
                    } else if (((String[])return_array.get(1))[1].equals(months)){
                        return_array.add(2, new String[] {"00", days});
                    } else if (((String[])return_array.get(1))[1].equals(days)){
                        return_array.add(2, new String[] {"00", hours});
                    }else if (((String[])return_array.get(1))[1].equals(hours)){
                        return_array.add(2, new String[] {"00", mins});
                    }else if (((String[])return_array.get(1))[1].equals(mins)){
                        return_array.add(1, new String[] {"00", hours});
                    }

                } else {
                    return_array.add(new String[] {"00", order_array.get(actual)[1]});
                }

            } else if (count == 1) {
                return_array.add(1, new String[] {"00", hours});
                return_array.add(2, new String[] {"00", mins});
                return_array.add(3, new String[] {"00", secs});
            }

            count = return_array.size();

        }

        return_array.add(Double.toString(initial_diff));
        return_array.add(timestamp);

        return return_array;

    }

    private void post(String path, final Map<String, String> params, ArrayList<Image> images, final SuccessfulBlock success, final FailedBlock failed) {
        String fullUrl = urlWithPath(path);

        System.out.println("call " + fullUrl);

        StringRequest postRequest = new StringRequest(Request.Method.POST, fullUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            success.success(new JSONObject(response));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        failed.failed(error);

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return params;

            }
        };
        Volley.newRequestQueue(context).add(postRequest);


    }

    private String urlWithPath(String path) {
        return BASE_HOST_URL + "/" + path;
    }

    public interface SuccessfulBlock {
        void success(JSONObject response) throws JSONException;
    }

    public interface FailedBlock {
        void failed(Object response);
    }


}
