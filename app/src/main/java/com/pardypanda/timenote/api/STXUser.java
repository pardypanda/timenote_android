package com.pardypanda.timenote.api;

import org.json.JSONException;
import org.json.JSONObject;

public class STXUser {
	private static STXUser sharedInstance = null;
	
	public String userID;
	public String username;
	public String fullname;
	public String profilePictureURL;

	public STXUser() {

	}

	public STXUser(JSONObject dictionary) {
		try {
			userID = dictionary.getString("id");
			username = dictionary.getString("username");
			fullname = dictionary.getString("full_name");
			profilePictureURL = dictionary.getString("profile_picture");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static STXUser sharedInstance() {
		if (sharedInstance == null)
		{
			sharedInstance = new STXUser();
		}
		
		return sharedInstance;
	}
	
	public void initWithDictionary(JSONObject dictionary)
	{
		if (sharedInstance == null)
		{
			sharedInstance = new STXUser();
		}
		
		try {
			sharedInstance.userID = dictionary.getString("id");
			sharedInstance.username = dictionary.getString("username");
			sharedInstance.fullname = dictionary.getString("full_name");
			sharedInstance.profilePictureURL = dictionary.getString("profile_picture");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int hash()
	{
		return sharedInstance.userID.hashCode();
	}
	
	public Boolean isEqualToUser(STXUser user)
	{
		return user.userID.equals(sharedInstance.userID);
	}
	
	public Boolean isEqual(STXUser user)
	{
		if (this == user)
			return true;
		
		return isEqualToUser(user);
	}
	
	public String description()
	{
		JSONObject dictionary = new JSONObject();
		try {
			if (sharedInstance.userID != null)
				dictionary.put("userID", sharedInstance.userID);
			else
				dictionary.put("userID", "");
			
			if (sharedInstance.username != null)
				dictionary.put("username", sharedInstance.username);
			else
				dictionary.put("username", "");
			
			if (sharedInstance.fullname != null)
				dictionary.put("fullname", sharedInstance.fullname);
			else
				dictionary.put("fullname", "");
			
			if (sharedInstance.profilePictureURL != null)
				dictionary.put("profilePictureURL", sharedInstance.profilePictureURL);
			else
				dictionary.put("profilePictureURL", "");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dictionary.toString();
	}
}
